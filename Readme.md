### English ###
---

Before beginning solving the challenge, please make sure you have forked 
the project and that you give the user of this repo access to the fork.

Your task is to develop a user interface for a music playlist. 
The playlist is made of one or more objects that represent a song.
The user should be able to search, edit, insert new and delete songs.

The Song object should at least have the following fields:

- id
- name
- artist
- album
- length

For the UI actions there is a REST API you can use. Please make sure you have
the software `npm` installed on your computer and that you have run the command

`npm install` 

in the root of the project folder.

The Server API can be then started with the command:

`npm start`

After you have started the server you can use the following REST-API endpoints:


`GET http://localhost:3000/music/`

`POST http://localhost:3000/music/`

`DELETE http://localhost:3000/music/:id`

`GET http://localhost:3000/music/:id`

Please take note of the following

- Code structure and organisation is very important.
- Best practices for javascript programming.
- We need expert Javascript developers.
- Design should be cool, but not take more than 20% of the time you spend for the challenge.
- Only HTML, CSS3/SASS, Javascript and jQuery is allowed.
- Use the folder `www` to implement your changes. 
- You can can make changes to the server.
- Don't be too shy when using git. Commit regularly.

### German ###
---

Bevor Sie mit der Aufgabe anfangen, stellen Sie bitte sicher, dass Sie
die Repository geforked haben. Zu Ihrem Fork muss der Benutzer dieser Repo zugriff haben.

Ihre Aufgabe ist die Erstellung einer Benutzeroberfläche für eine 
Musiksammlung (Playlist). Das Playlist kann mehrere Objekte beinhalten,
die jeweils einen Musikstück (Song) darstellen. Es soll die Möglichkeit 
geben im Playlist nach Musikstücke suchen, diese bearbeiten oder 
löschen und neue hinzufügen können.

Ein `Song` Objekt beinhaltet folgende Felder:

- id
- song
- artist
- album
- length

Für die Aktionen, die ihre Oberfläche vornehmen soll, stellen wir
Ihnen eine REST-API zu Verfügung. 
 
Bitte stellen Sie sicher, dass sie die Software npm auf Ihrem System
installiert haben und das Sie folgendem Befehl im Projektordner
aufgerufen haben:

`npm install`

Den Server für die API starten Sie mit dem Befehl 

`npm start`

Nachdem Sie den Server gestartet haben, stehen Ihnen folgende REST 
API-Endpunkte bereit

`GET http://localhost:3000/music/`

`POST http://localhost:3000/music/`

`DELETE http://localhost:3000/music/:id`

`GET http://localhost:3000/music/:id`


Zum Schluss einige wichtige Details:

- Achten Sie auf Code Struktur und Organization
- Setzen Sie auf die Best practice für Javascript
- Schwerpunkt ist die Programmierung mit Javascript
- Design kann schön sein, sollte nicht mehr als 20% der Entwicklungszeit nehmen
- Nur HTML, CSS3/SASS, Javascript und jQuery einsetzen
- Benutzen Sie den www Ordner für Ihre Oberfläche
- Das Quellcodeverwaltungssystem sollten Sie nicht vernachlässigen

Für Fragen und Unklarheiten stehen wir Ihnen jederzeit zur Verfügung.